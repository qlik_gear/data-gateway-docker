FROM oraclelinux:8.6

# Disable SELinux
#COPY ./config /etc/selinux

# Download gateway from github
RUN yum -y update
RUN yum install -y wget curl
RUN dnf clean packages
RUN dnf install -y python3
RUN dnf install -y python39-pip
RUN pip3 install requests
# Configure gateway
WORKDIR /app
RUN touch /app/key.json
COPY ./odbcinst.ini /etc/
COPY ./site_arep_login.sh .

#COPY ./install.py ./
#RUN python3 ./install.py

# Configure drivers

## PostgreSQL
RUN dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
RUN yum install -y postgresql13-odbc unixODBC

## Snowflake
RUN wget --no-verbose https://sfc-repo.snowflakecomputing.com/odbc/linux/2.24.0/snowflake-odbc-2.24.0.x86_64.rpm
RUN yum -y --quiet localinstall  ./snowflake-odbc-2.24.0.x86_64.rpm
## Databricks
RUN yum install zip unzip -y 
RUN wget -O db.zip --no-verbose https://databricks-bi-artifacts.s3.us-east-2.amazonaws.com/simbaspark-drivers/odbc/2.6.26/SimbaSparkODBC-2.6.26.1045-LinuxRPM-64bit.zip 
RUN unzip db.zip
RUN yum -y --quiet localinstall  ./simbaspark-2.6.26.1045-1.x86_64.rpm

## MySQL
RUN yum install -y --quiet https://repo.mysql.com/mysql80-community-release-el8-4.noarch.rpm
RUN yum-config-manager --enable mysql80-community mysql-tools-community mysql-connectors-community
RUN yum install mysql-connector-odbc-8.0.23 -y


## GBQ
RUN wget -O SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux.tar.gz https://storage.googleapis.com/simba-bq-release/odbc/SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux.tar.gz
RUN tar --directory=/opt -zxvf SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux.tar.gz
RUN cd /opt/SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux/ && tar xvf SimbaODBCDriverforGoogleBigQuery64_2.3.3.1005.tar.gz

RUN mv /opt/SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux/GoogleBigQueryODBC.did /opt/SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux/SimbaODBCDriverforGoogleBigQuery64_2.3.3.1005/lib/
RUN mv /opt/SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux/setup/simba.googlebigqueryodbc.ini /opt/SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux/SimbaODBCDriverforGoogleBigQuery64_2.3.3.1005/lib/
RUN sed -i 's/ErrorMessagesPath.*/ErrorMessagesPath=\/opt\/SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux\/SimbaODBCDriverforGoogleBigQuery64_2.3.3.1005\/ErrorMessages/' /opt/SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux/SimbaODBCDriverforGoogleBigQuery64_2.3.3.1005/lib/simba.googlebigqueryodbc.ini 
RUN sed -i 's/UTF-32/UTF-16/'  /opt/SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux/SimbaODBCDriverforGoogleBigQuery64_2.3.3.1005/lib/simba.googlebigqueryodbc.ini 


## Synapse
RUN curl https://packages.microsoft.com/config/rhel/8/prod.repo > /etc/yum.repos.d/mssql-release.repo
RUN yum remove unixODBC-utf16 unixODBC-utf16-devel
RUN yum -y update
#RUN ACCEPT_EULA=Y yum install -y msodbcsql18

#
# Start replicate agent when container starts
#CMD /etc/rc.d/init.d/repagent start && tail -F /opt/qlik/gateway/movement/data/logs/repagent.log
COPY ./init.sh .
RUN chmod +x /app/init.sh
CMD /app/init.sh $TENANT_URL

