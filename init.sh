#!/bin/bash

TENANT_URL=$1
echo "TENANT_URL=$TENANT_URL"

if [ -e /app/.executed ]
then
    echo "Gateway already installed. This is your Key:"
    echo ""
    cat /opt/qlik/gateway/movement/data/logs/key.json

    /etc/rc.d/init.d/repagent stop
    
else

    wget -O /app/qlik-data-gateway-data-movement.rpm --no-verbose https://github.com/qlik-download/saas-download-links/releases/download/qcs/qlik-data-gateway-data-movement.rpm
    # Install python for install script
    # Install the gateway
    QLIK_CUSTOMER_AGREEMENT_ACCEPT=yes rpm -i qlik-data-gateway-data-movement.rpm

    /opt/qlik/gateway/movement/bin/agentctl qcs set_config --tenant_url $TENANT_URL
    /opt/qlik/gateway/movement/bin/agentctl qcs get_registration > /opt/qlik/gateway/movement/data/logs/key.json
    touch /app/.executed

    

    echo "This is your Key:"
    echo ""
    cat /opt/qlik/gateway/movement/data/logs/key.json

fi

/etc/rc.d/init.d/repagent start && tail -F /opt/qlik/gateway/movement/data/logs/repagent.log
