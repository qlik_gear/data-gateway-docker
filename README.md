# Q Gateway Docker Image #

__IMPORTANT: THIS IMAGE IS JUST A PERSONAL PLAY AND IS NOT OFFICIALLY SUPPORTED BY QLIK__ . Gitlab repository: https://gitlab.com/qlik_gear/data-gateway-docker

## Default Run ##

Replace with your Tenant_URL.

```
docker run --name qgateway -d -e TENANT_URL=manuel-romero.eu.qlikcloud.com --network host mjromper/q-gateway 
```

- Use __--network host__ for the Gateway to see/reach same databases as host machine does.
- It will automatically install the latest version of the Qlik Gateway in the running container and generate registration key for the given TENANT_URL. The gateway service will start right away.
- Configured with ODBC drivers for: Postgres, MySQL, Databricks, Snowflake, Synapse, Google Big Query connectivity.

All you need to do is register this Gateway in your tenant management console using the produced registration key.

## Get registration key ##
IMPORTANT: 
 - It takes a couple minutes for the Gateway to be installed and initialised, so please wait a bit to get your registration key.

Option 1 (directly using agentctl)
```
docker exec -i qgateway /opt/qlik/gateway/movement/bin/agentctl qcs get_registration
```

Option 2 (since key is also stored in /opt/qlik/gateway/movement/data/logs/key.json)
```
docker exec -i qgateway cat /opt/qlik/gateway/movement/data/logs/key.json
```

## Run and expose logs and registration key to a folder in your Host machine ##
```
mkdir -p logs
docker run --name qgateway -d --network host -v $PWD/logs:/opt/qlik/gateway/movement/data/logs -e TENANT_URL=manuel-romero.eu.qlikcloud.com mjromper/q-gateway 
```
