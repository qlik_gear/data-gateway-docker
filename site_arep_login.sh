# Mysql ODBC lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib64/

# Postgres ODBC lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/pgsql-13/lib/

# Oracle ODBC lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/oracle/instantclient_19_16/

# Azure Synapse ODBC lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/microsoft/msodbcsql18/lib64/

# GoogleBigQuery ODBC lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/SimbaODBCDriverforGoogleBigQuery_2.3.3.1005-Linux/SimbaODBCDriverforGoogleBigQuery64_2.3.3.1005/lib/

# Databricks ODBC lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/simba/spark/lib/64/

# Snowflake ODBC lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib64/snowflake/odbc/lib/
